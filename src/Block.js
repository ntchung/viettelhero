import ui.ImageView as ImageView;
import math.geom.Vec2D as Vec2D;

import src.Global as Global;
import src.Effects as Effects;
import src.Bullet as Bullet;
import src.cp as cp;

var v = cp.v;

exports = Class(ImageView, function (supr) {

	this.init = function (opts) {

		supr(this, "init", [opts]);

		this.reset();
	};

	this.reset = function () {		
		this.startingY = this.style.y;
		
		this.HP = this._opts.startingHP;
		this.holder = this._opts.tower;
	};

	this.addBody = function () {
		var space = Global.space;

		var x = this.style.x;
		var y = Global.fieldHeight - this.style.y;
		var hw = this.style.width / 2 - this._opts.padding;
		var hh = this.style.height / 2 - this._opts.padding;

		var body = new cp.Body(Infinity, Infinity);
		//body.nodeIdleTime = Infinity;
		body.setPos(v(x, y));
		
		var box = space.addShape(new cp.BoxShape2(body, new cp.BB(-hw, -hh, hw, hh)));
		box.setElasticity(1);
		box.setFriction(1);
		box.setCollisionType(Global.ENEMY_BLOCK);
		box.setLayers(Global.ENEMY_BLOCK);

		this.shape = box;
		box.actor = this;
	};

	this.removeBody = function () {
		var space = Global.space;

		space.removeShape(this.shape);
		this.shape = null;
	};
	
	this.destroy = function() {
		this.removeBody();
		blocksPool.releaseView(this);
	};

	this.update = function (dt) {
		if (this.holder && !this.holder.style.visible) {
			this.holder = null;
		}
		
		if (this.holder) {
			this.style.y = this.holder.style.y - this.holder.style.height / 2 - this.style.height / 2 + this.holder._opts.topPadding + this._opts.bottomPadding;
			this.shape.body.p.y = Global.fieldHeight - this.style.y;
		}
		
		if (this.HP <= 0) {
			Effects.explode3(this, 0, 0, 1.0);
			this.destroy();	
		}		
	};
	
	this.scroll = function (dist) {
		this.style.x += dist;
		this.shape.body.p.x = this.style.x;		
	};

	this.damage = function (value) {
		Effects.pulse(this, {
			magnitude: 0.02
		});
		this.HP -= value;
	}
});
