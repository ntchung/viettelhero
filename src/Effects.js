import animate;
import ui.View;
import ui.ImageView as ImageView;

// math helper constants and functions
var PI = Math.PI;
var TAU = 2 * PI;
var abs = Math.abs;
var min = Math.min;
var max = Math.max;
var sin = Math.sin;
var cos = Math.cos;
var pow = Math.pow;
var floor = Math.floor;
var random = Math.random;
var choose = function (a) { 
  var n = Math.floor(Math.random() * a.length);
  return a[Math.min(n, a.length - 1)]; 
};
var rollFloat = function (n, x) { return n + random() * (x - n); };
var rollInt = function (n, x) { return floor(n + random() * (1 + x - n)); };

var particlesEngine = null;

/**
 * PARTICLES EFFECTS
 **/
exports.registerParticlesEngine = function(engines) {
  particlesEngine = engines;
};

exports.runParticlesEngine = function(dt) {
  for (var i = particlesEngine.length - 1; i >= 0; --i) {
    particlesEngine[i].runTick(dt);
  }
}

/**
 * Common emitter
 **/
var emitTextPanel = function(x, y, textImage, scale, engineIndex) {    
  var ttl = 500;
  var size = 50 * 2.2 * scale;
  x -= (size) * 0.5;
  y -= (size) * 0.5; 
  var stop = -1000 / ttl;  
  
  var data = particlesEngine[engineIndex].obtainParticleArray(1);
  var p = data[0];
  p.x = p.ox = x;
  p.y = p.oy = y;
  p.anchorX = size / 2;
  p.anchorY = size / 2;
  p.width = size;
  p.height = size;
  p.scale = 1.0;
  p.dscale = 0.1;
  p.ddopacity = 4 * stop;
  p.ttl = ttl;
  p.image = textImage;
  p.compositeOperation = "";
  
  particlesEngine[engineIndex].emitParticles(data);
};

/**
 * Explosion 1 - a boom
 **/
var SMOKES = [
  "resources/images/particles/smoke1.png",
  "resources/images/particles/smoke2.png",
  "resources/images/particles/smoke3.png",
  "resources/images/particles/smoke4.png",
  ];

var COLOR_SMOKES = [
  "resources/images/particles/redsmoke1.png",
  "resources/images/particles/redsmoke2.png",
  "resources/images/particles/yellowsmoke1.png",
  "resources/images/particles/yellowsmoke2.png",
  ];

var COLOR_TRIANGLES = [
  "resources/images/particles/triangle2a.png",
  "resources/images/particles/triangle2b.png",
  "resources/images/particles/triangle2c.png",
  ];

var FIREWORK_STARS = [
  [
  "resources/images/particles/yellowstar1.png",
  "resources/images/particles/yellowstar2.png",
  ],
  [
  "resources/images/particles/bluestar1.png",
  "resources/images/particles/bluestar2.png",
  ],
  [
  "resources/images/particles/greenstar1.png",
  "resources/images/particles/greenstar2.png",
  ],
  [
  "resources/images/particles/orangestar1.png",
  "resources/images/particles/orangestar2.png",
  ],
  [
  "resources/images/particles/redstar1.png",
  "resources/images/particles/redstar2.png",
  ],
  ];

var explode1_emitWhiteLines = function(x, y, scale, engineIndex, ttl) {  
  var size = 50 * 0.15 * scale;  
  var stop = -1000 / ttl;  
  var speed = 0.75 * scale;
  
  var data = particlesEngine[engineIndex].obtainParticleArray(50);
  
  for (var i = 0; i < 50; ++i) {
    var p = data[i];
    p.polar = true;
    p.radius = 0.1 * scale;
    p.dradius = speed * rollFloat(300, 600);
    p.theta = TAU * random();
    p.r = p.theta + Math.PI / 2;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size;
    p.width = size;
    p.height = size;
    p.scaleX = 1.0;
    p.scaleY = 4.0;
    p.dscaleY = -8.0 * stop;
    p.opacity = 0.9;
    p.dopacity = 1.25 * stop;
    p.ttl = ttl;
    p.image = "resources/images/particles/roundedlines.png";
    p.compositeOperation = "lighter";
  }
  particlesEngine[engineIndex].emitParticles(data);
};

var explode1_emitFlares = function(x, y, scale, engineIndex) {    
  var data = particlesEngine[engineIndex].obtainParticleArray(40);  
  var speed = 0.01;
  
  // flare
  for (var i = 0; i < 40; ++i) {
    var size = 25 * (Math.random() * 0.5 + 0.5) * scale; 
    var ttl = Math.floor(Math.random() * 200) + 300;
    var stop = -1000 / ttl;  
    
    var p = data[i];
    p.polar = true;
    p.radius = 0.2;
    p.dradius = speed * rollFloat(800, 1000);
    p.theta = TAU * random();
    p.r = p.theta + Math.PI;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size;
    p.width = size;
    p.height = size;
    p.scaleX = 1.0;
    p.scaleY = 8.0 * scale;
    p.dscaleY = 1.0 * stop;
    p.ddscaleY = p.scaleY * stop;
    p.opacity = 1.0;
    p.dopacity = 1.1 * stop;
    p.ttl = ttl;
    p.image = choose(COLOR_TRIANGLES);
    p.compositeOperation = "lighter";
  }
  
  particlesEngine[engineIndex].emitParticles(data);
};

var explode1_emitParticles = function(x, y, scale, engineIndex) {
  
  var data = particlesEngine[engineIndex].obtainParticleArray(6);
  
  var ttl = 1500;
  var size = 25 * 1.0 * scale;
  var stop = -1000 / ttl;  
  var speed = 0.05 * scale;
  
  // smokes
  for (var i = 0; i < 6; ++i) {
    var p = data[i];
    p.polar = true;
    p.radius = 0.01 * scale;    
    p.dradius = speed * scale * rollFloat(800, 1000);
    p.theta = TAU * scale * random();
    p.r = TAU * scale * random();
    p.dr = rollFloat(-2, 2) * scale;
    p.ddr = stop * p.dr * scale;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size / 2;
    p.width = size;
    p.height = size;
    p.scale = 1.0 * scale;
    p.dscale = -3.0 * stop * scale;
    p.opacity = 1.0;
    p.dopacity = 1.5 * stop;
    p.ttl = ttl;
    
    p.image = (i & 1) == 0 ? choose(SMOKES) : choose(COLOR_SMOKES);
    p.compositeOperation = "lighter";
  }
  
  particlesEngine[engineIndex].emitParticles(data);  
};

exports.explode1 = function(view, offsetX, offsetY, scale) {
  var textImage = "resources/images/particles/boom.png";
  var vs = view.style;
  var vx = vs.x + offsetX;
  var vy = vs.y + offsetY;
  
  emitTextPanel(vx, vy, textImage, scale, 1);
  explode1_emitWhiteLines(vx, vy, scale, 0, 500);
  explode1_emitFlares(vx, vy, scale, 0);
  
  animate(particlesEngine[0])
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);}).wait(100)
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);}).wait(100)
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);});
};

exports.explode3 = function(view, offsetX, offsetY, scale) {  
  var textImage = "resources/images/particles/boom.png";
  var vs = view.style;
  var vx = vs.x + offsetX;
  var vy = vs.y + offsetY;
  
  //emitTextPanel(vx, vy, textImage, 0.7 * scale, 1);
  //explode1_emitWhiteLines(vx, vy, scale, 0, 200);
  explode1_emitFlares(vx, vy, 0.7 * scale, 0);
  
  animate(particlesEngine[0])
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);}).wait(100)
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);}).wait(100)
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);});
};

var explode2_emitBulkSmokes1 = function(x, y, scale, engineIndex) {
  
  var data = particlesEngine[engineIndex].obtainParticleArray(80);
      
  var speed = 0.11 * scale;
  
  // smokes
  for (var i = 0; i < 80; ++i) {
    var ttl = Math.random() * 400 + 800;  
    var size = 25 * (Math.random() * 0.4 + 0.4) * scale;
    var stop = -1000 / ttl;  
    
    var p = data[i];
    p.polar = true;
    p.radius = 0.01 * scale;    
    p.dradius = speed * rollFloat(800, 1000) * scale;
    p.theta = TAU * random();
    p.r = TAU * random();
    p.dr = rollFloat(-2, 2);
    p.ddr = stop * p.dr;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size / 2;
    p.width = size;
    p.height = size;
    p.scale = 1.0;
    p.dscale = -3.0 * stop;
    p.opacity = 1.0;
    p.dopacity = 1.5 * stop;
    p.ttl = ttl;
    
    p.image = choose(COLOR_SMOKES);
    p.compositeOperation = "lighter";
  }
  
  particlesEngine[engineIndex].emitParticles(data);  
};

exports.explode2 = function(view, offsetX, offsetY, scale) {
  var vs = view.style;
  var vx = vs.x + offsetX;
  var vy = vs.y + offsetY;
  
  explode1_emitWhiteLines(vx, vy, scale, 0, 500);  
  explode2_emitBulkSmokes1(vx, vy, scale, 1);
    
  animate(particlesEngine[0])
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);}).wait(100)
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);}).wait(100)
    .then(function() {explode1_emitParticles(vx, vy, scale, 0);});
};

/**
 * A puff of smoke
 **/
var smokepuff_emitPuffs = function(x, y, scale, engineIndex) {      
  var data = particlesEngine[engineIndex].obtainParticleArray(40);  
  var speed = 0.1 * scale;
  
  // flare
  for (var i = 0; i < 40; ++i) {
    var size = 25 * (Math.random() * 0.1 + 0.4) * scale; 
    var ttl = Math.floor(Math.random() * 300) + 300;
    var stop = -1000 / ttl;  
    
    var p = data[i];
    p.polar = true;
    p.radius = 0.01 * scale;
    p.dradius = speed * rollFloat(400, 1000);
    p.theta = TAU * random();
    p.theta - Math.PI / 2;
    p.r = TAU * random();
    p.dr = rollFloat(-2, 2);
    p.ddr = stop * p.dr;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size / 2;
    p.width = size;
    p.height = size;
    p.scale = 1.0;    
    p.dscale = -1.0 * stop;
    p.ddscale = p.scale * stop;
    p.opacity = 1.0;
    p.dopacity = 1.1 * stop;
    p.ttl = ttl;
    p.image = choose(SMOKES);
    p.compositeOperation = "lighter";
  }
  
  particlesEngine[engineIndex].emitParticles(data);
};

exports.smokePuff = function(view, scale) {
  var vs = view.style;  
  explode1_emitWhiteLines(vs.x, vs.y, scale, 0, 200);
  exports.smokePuffAt(vs.x, vs.y, scale);
};

exports.smokePuffAt = function(x, y, scale) {  
  smokepuff_emitPuffs(x, y, scale, 0);
};

/**
 * Flash ray
 **/

exports.flashRay = function(view, offsetX, offsetY) {
  var vs = view.style;  
  var x = vs.x + offsetX;
  var y = vs.y + offsetY; 
  
  var data = particlesEngine[0].obtainParticleArray(50);
  
  for (var i = 0; i < 50; ++i) {
    var size = 50 * (Math.random() * 0.4 + 0.2);
    var ttl = Math.floor(Math.random() * 500 + 5);
  
    var stop = -1000 / ttl;  
    
    var p = data[i];
    p.polar = true;
    p.radius = 0.0;
    p.theta = TAU * random();
    p.r = p.theta;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size;
    p.width = size;
    p.height = size;
    p.scaleX = 1.5;
    p.scaleY = 6.0;
    p.dscaleY = 1.0 * stop;
    p.ddscaleY = p.scaleY * stop;
    p.opacity = 1.0;
    p.dopacity = 1.2 * stop;
    p.ttl = ttl;
    p.image = "resources/images/particles/ray.png";
    p.compositeOperation = "lighter";
  }
  particlesEngine[0].emitParticles(data);  
};

/**
 * Fireworks
 **/
exports.fireworks = function(view, offsetX, offsetY) {
  var ttl = 300;
  var speed = 0.125;
  var stop = -1000 / ttl;  
  
  var vs = view.style;
  var x = vs.x + offsetX;
  var y = vs.y + offsetY;
  
  var data = particlesEngine[1].obtainParticleArray(40);
  
  var fwPars = choose(FIREWORK_STARS);
  
  for (var i = 0; i < 40; ++i) {
    var size = 30 * (Math.random() * 0.2 + 0.3);    
      
    var p = data[i];
    p.polar = true;
    p.radius = 0.0;
    p.dradius = speed * rollFloat(400, 1000) * stop;
    p.ddradius = 2.0 * stop;
    p.theta = TAU * random();
    p.theta - Math.PI / 2;
    p.r = TAU * random();
    p.dr = rollFloat(-8, 8);
    p.ddr = stop * p.dr;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size;
    p.width = size;
    p.height = size;    
    p.scale = 1.0;
    p.dscale = -0.4 * stop;
    p.ddscale = p.scaleY * stop;
    p.opacity = 1.0;
    p.dopacity = 1.2 * stop;
    p.ttl = ttl;
    p.image = choose(fwPars);
    p.compositeOperation = "lighter";
  }
  particlesEngine[1].emitParticles(data);
};

/**
 * Sky ray
 **/
exports.emit_skyray = function(x, y, scaleY) {  
  
  var data = particlesEngine[0].obtainParticleArray(5);
  
  for (var i = 0; i < 5; ++i) {
    var ttl = 500 + Math.random() * 500;
    var stop = -1000 / ttl;  
    
    var size = 50 * (Math.random() * 0.2 + 0.5);    
      
    var p = data[i];
    p.polar = false;    
    p.x = p.ox = x + Math.random() * 7 - 14;;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = 0;
    p.width = size;
    p.height = size;    
    p.scaleX = 1.0;
    p.scaleY = scaleY;
    p.opacity = 0.0;
    p.dopacity = -1.0 * stop;
    p.ddopacity = 1.0 * stop;
    p.ttl = ttl;    
    p.image = "resources/images/particles/skyray.png";
    p.compositeOperation = "lighter";
  }
  particlesEngine[0].emitParticles(data);
};

exports.skyray = function(x, y, toY) {      
  var scaleY = Math.abs(toY - y) / 24;
  this.emit_skyray(x, y, scaleY);
};


/**
 * Flash ray on UI stars
 **/
exports.flashRayUIStar = function(x, y) {
  var data = particlesEngine[2].obtainParticleArray(50);
  
  for (var i = 0; i < 50; ++i) {
    var size = 50 * (Math.random() * 0.4 + 0.2);
    var ttl = Math.floor(Math.random() * 500 + 5);
  
    var stop = -1000 / ttl;  
    
    var p = data[i];
    p.polar = true;
    p.radius = 0.0;
    p.theta = TAU * random();
    p.r = p.theta;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size;
    p.width = size;
    p.height = size;
    p.scaleX = 1.75;
    p.scaleY = 5.0;
    p.dscaleY = 1.0 * stop;
    p.ddscaleY = p.scaleY * stop;
    p.opacity = 1.0;
    p.dopacity = 1.2 * stop;
    p.ttl = ttl;
    p.image = "resources/images/particles/ray.png";
    p.compositeOperation = "lighter";
  }
  particlesEngine[2].emitParticles(data);  
};

/**
 * Fireworks on UI
 **/
exports.fireworksUI = function(x, y) {
  var ttl = 1500;
  var speed = 1.0;
  var stop = -1000 / ttl;  
  
  var data = particlesEngine[2].obtainParticleArray(80);
  
  var fwPars = choose(FIREWORK_STARS);
  
  for (var i = 0; i < 80; ++i) {
    var size = 50 * (Math.random() * 0.2 + 0.3);    
      
    var p = data[i];
    p.polar = true;
    p.radius = 0.0;
    p.dradius = speed * rollFloat(400, 1000) * stop;
    p.ddradius = 4.0 * stop;
    p.theta = TAU * random();
    p.theta - Math.PI / 2;
    p.r = TAU * random();
    p.dr = rollFloat(-8, 8);
    p.ddr = stop * p.dr;
    p.x = p.ox = x;
    p.y = p.oy = y;    
    p.anchorX = size / 2;
    p.anchorY = size;
    p.width = size;
    p.height = size;    
    p.scale = 1.0;
    p.dscale = -0.4 * stop;
    p.ddscale = p.scaleY * stop;
    p.opacity = 1.0;
    p.dopacity = 1.2 * stop;
    p.ttl = ttl;
    p.image = choose(fwPars);
    p.compositeOperation = "lighter";
  }
  particlesEngine[2].emitParticles(data);
};


/**
 * OTHER EFFECTS
 **/

/**
 * shake a view rapidly, great for screen shaking like earthquakes
 * @param {ui.View} view
 * @param {Object} opts
 */
exports.shake = function (view, opts) {
  var ttl = opts.duration;
  var dt = ttl / 16;
  var m = 1.75 * opts.scale;
  var vs = view.style;
  var x = vs.x;
  var y = vs.y;
  var s = vs.scale;
  var ax = vs.anchorX;
  var ay = vs.anchorY;
  vs.anchorX = vs.width / 2;
  vs.anchorY = vs.height / 2;
  var r1 = TAU * random();
  var r2 = TAU * random();
  var r3 = TAU * random();
  var r4 = TAU * random();
  var r5 = TAU * random();
  var r6 = TAU * random();
  var r7 = TAU * random();
  var r8 = TAU * random();
  var r9 = TAU * random();
  var r10 = TAU * random();
  var r11 = TAU * random();
  var r12 = TAU * random();
  var r13 = TAU * random();
  var r14 = TAU * random();

  var anim = animate(view);
  anim.then({ scale: s * (1 + 0.05 * m) }, dt, animate.easeIn)
    .then({ x: x + 14 * m * cos(r1), y: y + 14 * m * sin(r1), scale: s * (1 + 0.046 * m) }, dt, animate.easeOut)
    .then({ x: x + 13 * m * cos(r2), y: y + 13 * m * sin(r2), scale: s * (1 + 0.042 * m) }, dt, animate.easeInOut)
    .then({ x: x + 12 * m * cos(r3), y: y + 12 * m * sin(r3), scale: s * (1 + 0.038 * m) }, dt, animate.easeInOut)
    .then({ x: x + 11 * m * cos(r4), y: y + 11 * m * sin(r4), scale: s * (1 + 0.034 * m) }, dt, animate.easeInOut)
    .then({ x: x + 10 * m * cos(r5), y: y + 10 * m * sin(r5), scale: s * (1 + 0.030 * m) }, dt, animate.easeInOut)
    .then({ x: x + 9 * m * cos(r6), y: y + 9 * m * sin(r6), scale: s * (1 + 0.026 * m) }, dt, animate.easeInOut)
    .then({ x: x + 8 * m * cos(r7), y: y + 8 * m * sin(r7), scale: s * (1 + 0.022 * m) }, dt, animate.easeInOut)
    .then({ x: x + 7 * m * cos(r8), y: y + 7 * m * sin(r8), scale: s * (1 + 0.018 * m) }, dt, animate.easeInOut)
    .then({ x: x + 6 * m * cos(r9), y: y + 6 * m * sin(r9), scale: s * (1 + 0.014 * m) }, dt, animate.easeInOut)
    .then({ x: x + 5 * m * cos(r10), y: y + 5 * m * sin(r10), scale: s * (1 + 0.010 * m) }, dt, animate.easeInOut)
    .then({ x: x + 4 * m * cos(r11), y: y + 4 * m * sin(r11), scale: s * (1 + 0.008 * m) }, dt, animate.easeInOut)
    .then({ x: x + 3 * m * cos(r12), y: y + 3 * m * sin(r12), scale: s * (1 + 0.006 * m) }, dt, animate.easeInOut)
    .then({ x: x + 2 * m * cos(r13), y: y + 2 * m * sin(r13), scale: s * (1 + 0.004 * m) }, dt, animate.easeInOut)
    .then({ x: x + 1 * m * cos(r14), y: y + 1 * m * sin(r14), scale: s * (1 + 0.002 * m) }, dt, animate.easeInOut)
    .then({ x: x, y: y, anchorX: ax, anchorY: ay, scale: s }, dt, animate.easeIn);
};

exports.squish = function (view, duration, scale) {
	var anim = view.getAnimation('basicEffects.squish');
	anim.commit()
	.then({scaleX: 0.88 * scale, scaleY: 1.12 * scale}, duration / 3, animate.easeOut)
	.then({scaleX: 1.06 * scale, scaleY: 0.94 * scale}, duration / 3, animate.easeIn)
	.then({scaleX: 1, scaleY: 1}, duration / 3, animate.easeIn);
	return anim;
};

exports.pulse = function (view, opts) {
	var orig = view.style.scale;
	var magnitude = opts && opts.magnitude || 0.5;
	var duration = opts && opts.duration || 500;
	var step = opts && opts.step || 100;
	var max = duration - step;

	var anim = view.getAnimation('basicEffects.pulse');
	var sign = 1;
	for (var t = 0; t < max; t += step) {
		anim.then({scale: orig + magnitude * sign * (max - t) / max}, step);
		sign *= -1;
	}

	anim.then({scale: orig}, step);
	return anim;
};
