import weeby;

import ui.TextView as TextView;
import ui.ImageView as ImageView;
import ui.View as View;
import ui.ViewPool as ViewPool;
import ui.ParticleEngine as ParticleEngine;
import ui.ScoreView as ScoreView;

import src.cp as cp;
import src.Global as Global;
import src.Hero as Hero;
import src.Bullet as Bullet;
import src.Effects as Effects;
import src.Enemy as Enemy;
import src.Tower as Tower;
import src.Block as Block;
import src.Level as Level;
import src.Background as Background;
import src.Floor as Floor;
import src.NumberFont as NumberFont;
import src.Sound as Sound;

exports = Class(weeby.Application, function () {

    var GameState = {
        Init: 0,
        Fight: 1,
        Clear: 2,
        GameOver: 3
    };

    var v = cp.v;
    
    this.initUI = function () {

        var self = this;

        Global.scaleScreen(this.gameView);
        
        var bgHeight = this.gameView.style.height;
        this.background = new Background({
            superview: this.gameView,
            x: 0,
            y: 0,
            width: this.gameView.style.width,
            height: bgHeight
        });

        GLOBAL.rootView = new View({
            superview: this.gameView,
            x: 0,
            y: 0,
            width: Infinity,
            height: Global.fieldHeight
        });
        
        this.setupPhysics();
        
        // Pools
        GLOBAL.bulletsPool = new ViewPool({
            ctor: Bullet,
            initCount: 100,
            initOpts: {
                superview: rootView,
                zIndex: 100
            }
        });

        GLOBAL.mortarsPool = new ViewPool({
            ctor: Enemy,
            initCount: 10,
            initOpts: {
                superview: rootView,
                zIndex: 50,
                url: 'resources/images/mortar/mortar',
                bulletImage: 'resources/images/bullets/bullet4-fly-0001.png',
                bulletSize: 30,
                bulletMaxBounces: 1,
                attackSound: 'rlaunch',
                width: 108,
                height: 58,
                offsetX: -32,
                offsetY: -58,
                frameRate: 5,
                collisionSize: 59,
                barrelX: -22,
                barrelY: 47, 
                mass: 120,
                launchRocketVerticalForce: 1000,
                waitBeforeAction: 3000,
                firstWaitBeforeAction: 2000,
                inaccuracy: 200,
                scoreValue: 1,
                instantDeath: false,
                poolIdx: 0
            }
        });
        
        GLOBAL.canonPool = new ViewPool({
            ctor: Enemy,
            initCount: 10,
            initOpts: {
                superview: rootView,
                zIndex: 50,
                url: 'resources/images/canon/canon',
                bulletImage: 'resources/images/bullets/bullet3-fly-0001.png',
                bulletSize: 24,
                bulletMaxBounces: 0,
                attackSound: 'flaunch',
                width: 118,
                height: 58,
                offsetX: -59,
                offsetY: -58,
                frameRate: 5,
                collisionSize: 59,
                barrelX: -22,
                barrelY: 47, 
                mass: 80,
                launchRocketVerticalForce: 500,
                waitBeforeAction: 2000,
                firstWaitBeforeAction: 2000,
                inaccuracy: 200,
                scoreValue: 2,
                instantDeath: false,
                poolIdx: 1
            }
        });
        
        GLOBAL.tanksPool = new ViewPool({
            ctor: Enemy,
            initCount: 10,
            initOpts: {
                superview: rootView,
                zIndex: 50,
                url: 'resources/images/tank/tank',
                bulletImage: 'resources/images/bullets/bullet5-fly-0001.png',
                bulletSize: 24,
                bulletMaxBounces: 2,
                attackSound: 'iceball',
                width: 96,
                height: 78,
                offsetX: -48,
                offsetY: -78,
                frameRate: 5,
                collisionSize: 84,
                barrelX: -30,
                barrelY: 67, 
                mass: 500,
                launchRocketVerticalForce: 500,
                waitBeforeAction: 2000,
                firstWaitBeforeAction: 1000,
                inaccuracy: 300,
                scoreValue: 5,
                instantDeath: true,
                poolIdx: 2
            }
        });

        GLOBAL.enemiesPool = [mortarsPool, canonPool, tanksPool];

        GLOBAL.towersPool = new ViewPool({
            ctor: Tower,
            initCount: 30,
            initOpts: {
                superview: rootView,
                startingHP: 10,
                zIndex: 75
            }
        });
        
        GLOBAL.blocksPool = new ViewPool({
            ctor: Block,
            initCount: 30,
            initOpts: {
                superview: rootView,
                startingHP: 10,
                zIndex: 25
            }
        });
        
        // Main character
        this.hero = new Hero({
            x: Global.heroPositionX,
            y: Global.fieldHeight - Global.floorY,
            zIndex: 10
        });

        this.hero.addBody();

        var parEngineLow = new ParticleEngine({
            superview: rootView,
            width: Global.fieldWidth,
            height: Global.fieldHeight,
            centerAnchor: true,
            zIndex: 80
        });

        var parEngineAboveObjects = new ParticleEngine({
            superview: rootView,
            width: Global.fieldWidth,
            height: Global.fieldHeight,
            centerAnchor: true,
            zIndex: 150
        });
        
        this.floor = new Floor({
            superview: this.gameView,
            x: 0,
            y: 0,
            width: this.gameView.style.width,
            height: bgHeight
        });        
        
        // GUI starts here
        var bgScore = new ImageView({
            superview: this.gameView,
            x: 10,
            y: 10,
            width: 152,
            height: 152,
            image: 'resources/images/ui/score-background.png'
        });
        
        this.labelScore = new ScoreView({
            superview: bgScore,
            x: -2,
            y: 50,            
            width: 160,
            height: 48,
            text: "0",
            horizontalAlign: "center",
            verticalAlign: "middle",
            characterData: NumberFont.getScoreFontData(),            
        });
                
        this.bgPowerBar = new ImageView({
            superview: this.gameView,
            image: 'resources/images/ui/ui-power-1.png',
            x: 60,
            y: Global.fieldHeight - Global.floorY - 100,
            width: 96,
            height: 24,
            visible: false
        });
        
        this.powerBar = new View({
            superview: this.bgPowerBar,            
            x: 6,
            y: 5,
            width: 84,
            height: 12,
            clip: true 
        });
        
        this.powerBarContent = new ImageView({
            superview: this.powerBar,
            image: 'resources/images/ui/ui-power-2.png',
            x: 0,
            y: 0,
            width: this.powerBar.style.width,
            height: this.powerBar.style.height, 
        });
        
        var parEngineOverUI = new ParticleEngine({
            superview: this.gameView,
            width: Global.fieldWidth,
            height: Global.fieldHeight,
            centerAnchor: true,
            zIndex: 1000
        }); 
        
        // Overlay for catching touches
        var overlay = new View({
            superview: this.gameView,
            x: 0,
            y: 0,
            zIndex: 10,
            width: Infinity,
            height: Global.fieldHeight
        });

        var overlayTouched = false;

        overlay.on('InputStart', function (event, point) {
            if (self.state == GameState.Fight && !self.hero.isDying()) {
                overlayTouched = true;
               
                self.hero.startCharging();
                self.hero.lookAt(point);
                self.powerBar.style.width = 0;
                self.bgPowerBar.style.visible = true;
            }            
        });

        overlay.on('InputMove', function (event, point) {
            if (overlayTouched && self.state == GameState.Fight && !self.hero.isDying()) {
                self.hero.lookAt(point);
            }
        });

        overlay.on('InputOut', function (over, overCount, atTarget) {
            if (self.state == GameState.Fight && !self.hero.isDying()) {
                overlayTouched = false;
                
                self.hero.attack();                
            }
            
            self.bgPowerBar.style.visible = false;            
        });
        
        this.airStrikeButton = new ImageView({
            superview: this.gameView,
            zIndex: 100,
            image: 'resources/images/ui/ui-time-plane-1.png',
            width: 196,
            height: 89,
            x: Global.fieldWidth - 216,
            y: 20,
        });
        
        this.airStrikeTimer = new View({
            superview: this.airStrikeButton,
            width: 108,
            height: 24,
            x: 14,
            y: 32,
            clip: true            
        });
        
        var airStrikeTimerContent = new ImageView({
            superview: this.airStrikeTimer,
            image: 'resources/images/ui/ui-time-plane-3.png',
            x: 0,
            y: 0,
            width: this.airStrikeTimer.style.width,
            height: this.airStrikeTimer.style.height
        });        
        
        this.airStrikeButton.on('InputSelect', function() {            
            if (self.airStrikeTimeOut <= 0 && self.state == GameState.Fight) {
                self.hero.launchAirStrike();
                self.airStrikeTimeOut = Global.AirStrikeTime;
            } 
        });
        
        // Register effects engines
        Effects.registerParticlesEngine([parEngineLow, parEngineAboveObjects, parEngineOverUI]);
        
        Sound.init();  
        this.gameView.tick = bind(this, 'onTick');
        this.gameView.style.visible = false;  
        
        this.themeIdx = 0;
    }; 

    this.launchUI = function (opts) {    
    };
    
    this.startGame = function (gameData) {
        bulletsPool.forEachActiveView(function (view, index) {
            view.destroy();
        });

        for (var i = enemiesPool.length - 1; i >= 0; --i) {
            enemiesPool[i].forEachActiveView(function (view, index) {
                view.destroy();
            });
        }

        towersPool.forEachActiveView(function (view, index) {
            view.destroy();
        });
        
        blocksPool.forEachActiveView(function (view, index) {
            view.destroy();
        });   
        
        this.state = GameState.Init;
        this.level = 0;
        this.score = 0;
        this.airStrikeTimeOut = Global.AirStrikeTime;
        this.airStrikeTimer.style.width = 0;

        this.scrollDistance = 0;
        this.scrollX = 0;
        
        this.bgPowerBar.style.visible = false;
        this.labelScore.setText('0');
        
        this.themeIdx = Math.floor(Math.random() * 1.5);        
        this.background.reset(this.themeIdx);
        this.floor.reset(this.themeIdx);
        this.hero.revive();  
    };
    
    this.onTick = function (dt) {
        var self = this;
        Effects.runParticlesEngine(dt);

        dt = Math.min(dt, 30);

        bulletsPool.forEachActiveView(function (view, index) {
            view.update(dt);
        });

        var enemiesCount = 0;
        for (var i = enemiesPool.length - 1; i >= 0; --i) {
            enemiesPool[i].forEachActiveView(function (view, index) {
                view.update(dt);
                ++enemiesCount;
            });
        }

        towersPool.forEachActiveView(function (view, index) {
            view.update(dt);
        });
        
        blocksPool.forEachActiveView(function (view, index) {
            view.update(dt);
        });

        if (this.state == GameState.Fight && this.hero.isDying()) {
            this.bgPowerBar.style.visible = false;
            this.hero.update(dt);   
            
            if (this.hero.isVanquished()) {                              
                this.onFinishGame({
                    score: self.score,                   
                    levelComplete: false
                });        
                
                this.state = GameState.GameOver;
            }            
        } else {
            
            this.airStrikeTimer.style.width = (Global.AirStrikeTime - this.airStrikeTimeOut) / Global.AirStrikeTime * 108; 
            
            if (this.airStrikeTimeOut > 0) {
                this.airStrikeTimeOut -= dt;
                this.airStrikeButton.setImage('resources/images/ui/ui-time-plane-1.png');
            }
            else {
                this.airStrikeButton.setImage('resources/images/ui/ui-time-plane-2.png');
            }
            
            switch (this.state) {
                case GameState.Init:
                    Level.setupLevel(this.level, Global.fieldWidth);
                    this.state = GameState.Clear;
                    this.scrollDistance = Global.fieldWidth / 2;

                    this.hero.startAnimation('run', { loop: true });
                    this.hero.style.zIndex = 1000;
                    this.hero.touchTime = 0;
                    this.airStrikeTimer.style.width = 0;
                    this.bgPowerBar.style.visible = false;
                    break;

                case GameState.Fight:                    
                    this.powerBar.style.width = Math.min(this.hero.touchTime, 1000) * this.powerBar._opts.width / 1000;
                
                    if (enemiesCount <= 0 && !this.hero.isDying()) {
                        ++this.level;
                        Level.setupLevel(this.level, Global.fieldWidth);

                        this.state = GameState.Clear;
                        this.bgPowerBar.style.visible = false;
                        this.scrollDistance = 0;

                        this.hero.startAnimation('run', { loop: true });
                        this.hero.style.zIndex = 1000;
                    }

                    this.hero.style.x = Global.heroPositionX;
                    this.hero.update(dt);
                    
                    break;

                case GameState.Clear:
                    var dist = dt * 0.001 * 300;

                    if (this.scrollDistance + dist > Global.fieldWidth) {
                        this.hero.touchTime = 0;
                        this.hero.startAnimation('idle1', { loop: true });
                        this.hero.style.zIndex = 10;                     
                        
                        // Re-scroll everything
                        this.hero.style.x = Global.heroPositionX;

                        bulletsPool.forEachActiveView(function (view, index) {
                            view.scroll(rootView.style.x);
                        });

                        for (var i = enemiesPool.length - 1; i >= 0; --i) {
                            enemiesPool[i].forEachActiveView(function (view, index) {
                                view.scroll(rootView.style.x);
                            });
                        }

                        towersPool.forEachActiveView(function (view, index) {
                            if (view.style.x + view.style.width < Global.fieldWidth) {
                                view.destroy();
                            } else {
                                view.scroll(rootView.style.x);
                            }
                        });
                        
                        blocksPool.forEachActiveView(function (view, index) {
                            if (view.style.x + view.style.width < Global.fieldWidth) {
                                view.destroy();
                            } else {
                                view.scroll(rootView.style.x);
                            }
                        });

                        rootView.style.x = 0;

                        this.state = GameState.Fight;
                    } else {
                        this.background.update(-dist);
                        this.floor.update(-dist);
                        
                        this.scrollDistance += dist;

                        this.hero.style.x = this.scrollDistance + Global.heroPositionX;
                        rootView.style.x = -this.scrollDistance;
                    }
                    
                    break;
            }
        }

        this.space.step(dt * 0.001);
    };

    this.isFighting = function () {
        return this.state == GameState.Fight && !this.hero.isDying();
    };

    this.setupPhysics = function () {
        var self = this;
        var space = this.space = Global.space;
        Global.addFloor();

        // Hero Bullets vs Walls
        space.addCollisionHandler(Global.HERO_BULLET, Global.WALL,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                var a = arb.getShapes()[0];
                a.actor.collideWithWall();
                
                //Sound.play('explosion_dull');
                
                return false;
            },
            function (arb, space) {
                return true;
            });
            
        // Hero Bullets vs themselves
        space.addCollisionHandler(Global.HERO_BULLET, Global.HERO_BULLET,
            function (arb, space) {
                return false;
            },
            function (arb, space) {
                return false;
            },
            function (arb, space) {            
                return false;
            },
            function (arb, space) {
                return false;
            });
            
        // Hero Bullets vs Enemies
        space.addCollisionHandler(Global.HERO_BULLET, Global.ENEMY,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                var a = arb.getShapes()[0];
                var b = arb.getShapes()[1];
                a.actor.explode();

                var dx = b.actor.style.x - a.actor.style.x;
                b.actor.applyImpulse(v(dx * 200, 15000));
                
                Sound.play('explodemini');
                return false;
            },
            function (arb, space) {
                return true;
            });
            
        // Enemies Bullets vs Walls
        space.addCollisionHandler(Global.ENEMY_BULLET, Global.WALL,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                var a = arb.getShapes()[0];
                a.actor.collideWithWall();
                
                //Sound.play('explosion_dull');
                return false;
            },
            function (arb, space) {
                return true;
            });
            
        // Enemies bullet vs hero
        space.addCollisionHandler(Global.ENEMY_BULLET, Global.HERO,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                if (self.isFighting()) {
                    var a = arb.getShapes()[0];
                    var b = arb.getShapes()[1];
                    a.actor.explode();

                    var dx = b.actor.style.x - a.actor.style.x;
                    b.actor.applyImpulse(v(dx * 200, 15000));
                    
                    Sound.play('explodemini');
                    return false;
                }
            },
            function (arb, space) {
                return true;
            });
            
        // Hero bullets vs Enemies bullets
        space.addCollisionHandler(Global.HERO_BULLET, Global.ENEMY_BULLET,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                var a = arb.getShapes()[0];
                var b = arb.getShapes()[1];
                a.actor.explode();
                b.actor.explode();
                
                //Sound.play('explosion_dull');
                return false;
            },
            function (arb, space) {
                return true;
            });
            
        // Enemy Bullets vs themselves
        space.addCollisionHandler(Global.ENEMY_BULLET, Global.ENEMY_BULLET,
            function (arb, space) {
                return false;
            },
            function (arb, space) {
                return false;
            },
            function (arb, space) {            
                return false;
            },
            function (arb, space) {
                return false;
            });
            
        // Hero bullets vs Enemies towers
        space.addCollisionHandler(Global.HERO_BULLET, Global.ENEMY_TOWER,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                var a = arb.getShapes()[0];
                var b = arb.getShapes()[1];
                a.actor.collideWithWall();
                b.actor.damage(1);
                Effects.smokePuff(a.actor, 0.5);
                
                Sound.play('rumble');
                return false;
            },
            function (arb, space) {
                return true;
            });
            
        // Hero bullets vs Enemies blocks
        space.addCollisionHandler(Global.HERO_BULLET, Global.ENEMY_BLOCK,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                var a = arb.getShapes()[0];
                var b = arb.getShapes()[1];
                a.actor.collideWithWall();
                b.actor.damage(1);
                Effects.smokePuff(a.actor, 0.5);
                
                Sound.play('rumble');
                return false;
            },
            function (arb, space) {
                return true;
            });
            
        // Enemies vs wall
        space.addCollisionHandler(Global.ENEMY, Global.WALL,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                var a = arb.getShapes()[0];
                a.actor.collideWithWall();                
                return false;
            },
            function (arb, space) {
                return true;
            });
            
        // Enemies vs Enemies towers
        space.addCollisionHandler(Global.ENEMY, Global.ENEMY_TOWER,
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                return true;
            },
            function (arb, space) {
                var a = arb.getShapes()[0];
                a.actor.collideWithWall();                               
                return false;
            },
            function (arb, space) {
                return true;
            });
    };

    this.addScore = function (value) {
        this.score += value;
        this.labelScore.setText(this.score.toString());
        
        // <TODO> - update game-loops score
    };
});

