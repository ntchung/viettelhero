import ui.View as View;
import src.Global as Global;
import parallax.Parallax as Parallax;

var floor1 = [
    {
        id: "floor1",
        zIndex: 1,
        yMultiplier: 0,
        yCanSpawn: false,
        yCanRelease: false,
        xMultiplier: 0.75,
        xCanSpawn: true,
        xCanRelease: true,    
        pieceOptions: [
            {
                id: "floor1",
                image: "resources/images/background_grass/floor.png"
            }
        ]
    },  
];

var floor2 = [
    {
        id: "floor2",
        zIndex: 1,
        yMultiplier: 0,
        yCanSpawn: false,
        yCanRelease: false,
        xMultiplier: 0.75,
        xCanSpawn: true,
        xCanRelease: true,    
        pieceOptions: [
            {
                id: "floor2",
                image: "resources/images/background_desert/floor.png"
            }
        ]
    },  
];

var floors = [floor1, floor2];

exports = Class(View, function (supr) {
	this.init = function (opts) {		
		supr(this, "init", [opts]);

		this.parallax = new Parallax({
			rootView: this
		});
	};
    
    this.reset = function (theme) {
        var bot = (this._opts.height) / this._opts.superview.style.scale  - Global.floorY;
        floor1[0].pieceOptions[0].y = bot;		
        floor2[0].pieceOptions[0].y = bot;
		this.parallax.reset(floors[theme]);
        
        this.position = 0;
    };
    
    this.update = function (x) {
        this.position += x;
        this.parallax.update(this.position, 0);
    };
});
