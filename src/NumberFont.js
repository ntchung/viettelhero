var scoreFontData = null;

exports.getScoreFontData = function() {
  
  if (scoreFontData == null) {
    var d = {};
	for (var i = 0; i < 10; i++) {
		d[i] = {
			image: "resources/images/ui/" + "score_" + i + ".png"
		};
	}
	
	d[","] = {
        image: "resources/images/ui/" + "score_comma.png"
    };
	
	d["x"] = {
        image: "resources/images/ui/" + "score_x.png"
    };
	
	scoreFontData = d;
  }
  
  return scoreFontData;
};
