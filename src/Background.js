import ui.View as View;
import ui.ImageView as ImageView;
import src.Global as Global;
import ui.ViewPool as ViewPool;
import parallax.Parallax as Parallax;

var parallax1 = [
	{
        id: "bg1",
        zIndex: 1,
        xMultiplier: 0,
        xCanSpawn: true,
        xCanRelease: true,
        yMultiplier: 0,
        yCanSpawn: false,
        yCanRelease: false,
        pieceOptions: [
            {
                id: "bg1",
                image: "resources/images/background_grass/sky.jpg",
                anchorY: 734,
                yAlign: 'bottom'
            }
        ]
    },
    {
        id: "farcastles",
        zIndex: 2,
        yMultiplier: 0,
        yCanSpawn: false,
        yCanRelease: false,
        xMultiplier: 0.2,
        xCanSpawn: true,
        xCanRelease: true,    
        pieceOptions: [
            {
                id: "farcastles1",
                image: "resources/images/background_grass/castle.png",
                yAlign: 'bottom',
                y: 547
            }
        ]
    },
    {
        id: "bush",
        zIndex: 4,
        yMultiplier: 0,
        yCanSpawn: false,
        yCanRelease: false,
        xMultiplier: 0.4,
        xCanSpawn: true,
        xCanRelease: true,    
        pieceOptions: [
            {
                id: "bush1",
                image: "resources/images/background_grass/tree.png",
                yAlign: 'bottom',
                y: 547
            }
        ]
    }
];

var parallax2 = [
	{
        id: "bg2",
        zIndex: 1,
        xMultiplier: 0,
        xCanSpawn: true,
        xCanRelease: true,
        yMultiplier: 0,
        yCanSpawn: false,
        yCanRelease: false,
        pieceOptions: [
            {
                id: "bg2",
                image: "resources/images/background_desert/sky.jpg",
                anchorY: 734,
                yAlign: 'bottom'
            }
        ]
    },
    {
        id: "farcastles",
        zIndex: 2,
        yMultiplier: 0,
        yCanSpawn: false,
        yCanRelease: false,
        xMultiplier: 0.2,
        xCanSpawn: true,
        xCanRelease: true,    
        pieceOptions: [
            {
                id: "farcastles1",
                image: "resources/images/background_grass/castle.png",
                yAlign: 'bottom',
                y: 547
            }
        ]
    },
    {
        id: "mountain",
        zIndex: 4,
        yMultiplier: 0,
        yCanSpawn: false,
        yCanRelease: false,
        xMultiplier: 0.4,
        xCanSpawn: true,
        xCanRelease: true,    
        pieceOptions: [
            {
                id: "mountain1",
                image: "resources/images/background_desert/mountain.png",
                yAlign: 'bottom',
                y: 547
            }
        ]
    }
];

var parallaxes = [parallax1, parallax2];

exports = Class(View, function (supr) {
	this.init = function (opts) {		
		supr(this, "init", [opts]);

		this.parallax = new Parallax({
			rootView: this
		});
		
        this.cloudsPool = new ViewPool({
            ctor: ImageView,
            initCount: 10,
            initOpts: {
                superview: this,
                zIndex: 3
            }
        });                    
	};
    
    this.reset = function(theme) {
        var viewScale = this._opts.superview.style.scale;
        var par = parallaxes[theme];
        var bot = (this._opts.height) / viewScale - Global.floorY;
        par[0].pieceOptions[0].scaleY = viewScale;
        par[0].pieceOptions[0].y = (this._opts.height) / viewScale;
        par[1].pieceOptions[0].y = bot;
        par[2].pieceOptions[0].y = bot;
        
		this.parallax.reset(par);
        this.parallax.update(0, 0);
        
        this.cloudsCounter = 0;        
        this.position = 0;   
    };
    
    this.update = function (x) {
        this.position += x;
        this.parallax.update(this.position, 0);
        
        this.cloudsPool.forEachActiveView(function (view, index) {
            view.style.x += x * 0.4;            
            this.cloudsCounter -= x * 0.4 / 0.01;
        });
    };
    
    this.tick = function (dt) {
        this.cloudsCounter -= dt;
        if (this.cloudsCounter <= 0) {
            this.cloudsCounter = 10000 + Math.random() * 5000;
            
            var cloud = this.cloudsPool.obtainView();
            cloud.updateOpts({ 
                x: Global.fieldWidth,
                y: Math.random() * Global.fieldHeight - 150,                
                visible: true,
            });
            cloud.setImage('resources/images/background_grass/cloud' + Math.floor(Math.random() * 2.9 + 1) + '.png', {autoSize: true});
        }
        
        var self = this;
        this.cloudsPool.forEachActiveView(function (view, index) {
            view.style.x -= dt * 0.01;
            if (view.style.x + view.style.width < 0) {
                self.cloudsPool.releaseView(view);
            }
        });
    };
});
