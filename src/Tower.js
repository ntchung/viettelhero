import ui.ImageView as ImageView;
import math.geom.Vec2D as Vec2D;

import src.Global as Global;
import src.Effects as Effects;
import src.Bullet as Bullet;
import src.cp as cp;

var v = cp.v;

exports = Class(ImageView, function (supr) {

	this.init = function (opts) {		
		supr(this, "init", [opts]);

		this.reset();
	};

	this.reset = function () {		
		this.startingY = this.style.y;
		
		this.HP = this._opts.startingHP;
	};

	this.addBody = function () {
		var space = Global.space;

		var topPadding = this._opts.topPadding;
		var x = this.style.x;
		var y = Global.fieldHeight - this.style.y - (topPadding / 2);
		var hw = this.style.width / 2 - this._opts.horizontalPadding;
		var hh = this.style.height / 2 - topPadding / 2;

		var body = new cp.Body(Infinity, Infinity);
		//body.nodeIdleTime = Infinity;
		body.setPos(v(x, y));
		
		var box = space.addShape(new cp.BoxShape2(body, new cp.BB(-hw, -hh, hw, hh)));
		box.setElasticity(1);
		box.setFriction(1);
		box.setCollisionType(Global.ENEMY_TOWER);
		box.setLayers(Global.ENEMY_TOWER);

		this.shape = box;
		box.actor = this;
	};

	this.removeBody = function () {
		var space = Global.space;

		space.removeShape(this.shape);
		this.shape = null;
	};
	
	this.destroy = function() {
		this.removeBody();
		towersPool.releaseView(this);
	};

	this.update = function (dt) {
		var fallY = this.startingY + (1.0 - (this.HP / this._opts.startingHP)) * this.style.height;
		
		if (Math.abs(this.style.y - fallY) > 1) {		
			this.style.y += (fallY - this.style.y) * Math.min(dt * 0.005, 1.0);
			this.shape.body.p.y = Global.fieldHeight - this.style.y - this._opts.topPadding / 2;
		} else {
			if (this.HP <= 0 || this.style.y + this.style.offsetY > Global.fieldHeight - Global.floorY) {
				this.destroy();	
			}
		}
	};
	
	this.scroll = function (dist) {
		this.style.x += dist;
		this.shape.body.p.x = this.style.x;		
	};

	this.damage = function (value) {
		Effects.pulse(this, {
			magnitude: 0.02
		});
		this.HP -= value;
		
		Effects.smokePuffAt(this.style.x, Global.fieldHeight - Global.floorY, 1.5);
	}
});
