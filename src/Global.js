import device;
import src.cp as cp;
var v = cp.v;

var fieldWidth = 960;
var floorY = 30;

exports.scaleScreen = function(view) {	
	view.style.scale = view.style.width / fieldWidth;	
	exports.fieldHeight = view.style.height / view.style.scale;
};

exports.bulletsCanBounce = false;
exports.heroPositionX = 100;
exports.minimumChargeToAttack = 300;
exports.attackCoolDown = 500;
exports.AirStrikeTime = 10000;

exports.fieldWidth = fieldWidth;
exports.fieldHeight = 540;
exports.floorY = floorY;

exports.WALL = 0xFFFFFFFF;
exports.HERO = 2;
exports.HERO_BULLET = 4 | 8 | 16 | 32 | 64;
exports.ENEMY = 8 | 32;
exports.ENEMY_BULLET = 16 | 4 | 2;
exports.ENEMY_TOWER = 32;
exports.ENEMY_BLOCK = 64;

var space = new cp.Space();
space.iterations = 60;
space.gravity = v(0, -500);
space.sleepTimeThreshold = 0.5;
space.collisionSlop = 0.5;
space.sleepTimeThreshold = 0.5;

exports.space = space;

exports.addFloor = function () {
	var floor = space.addShape(new cp.BoxShape2(space.staticBody, new cp.BB(-fieldWidth, -256, fieldWidth, floorY)));
	floor.setElasticity(1);
	floor.setFriction(1);
	floor.setCollisionType(exports.WALL);
	floor.setLayers(exports.WALL);
};

exports.devkit2chipmunks = function(view, offsetX, offsetY) {
	return v(view.style.x + offsetX, exports.fieldHeight - (view.style.y + offsetY));
};

exports.chipmunks2devkit = function(view, body, offsetX, offsetY) {
	view.style.x = body.p.x + offsetX;
	view.style.y = exports.fieldHeight - (body.p.y + offsetY);
};

exports.isBodyStopped = function(body) {
	return Math.abs(body.vx) < 0.001 && Math.abs(body.vy) < 0.001;	
};