import math.geom.Vec2D as Vec2D;

import src.Global as Global;
import src.Bullet as Bullet;
import src.Enemy as Enemy;
import src.Tower as Tower;

import src.cp as cp;
var v = cp.v;

exports.setupLevel = function (level, offsetX) {
	var levelData = levels[level % levels.length];
	var roundTrip = Math.floor(level / levels.length);
	var styleIdx = Math.floor(Math.random() * 3.9) + 1;

	for (var key in levelData) {
		var name = key.split('_')[0];
			
		switch (name) {
			case "tower":
				addTower(levelData[key], styleIdx, offsetX, roundTrip);
				break;
				
			case "block":
				addBlock(levelData[key], styleIdx, offsetX, roundTrip, null);
				break;
				
			case "mortar":
				addMortar(levelData[key], offsetX, roundTrip);
				break;
				
			case "canon":
				addCanon(levelData[key], offsetX, roundTrip);
				break;
				
			case "tank":
				addTank(levelData[key], offsetX, roundTrip);
				break;
		}
	}
};

var addMortar = function (data, offsetX, roundTrip) {
	var m = enemiesPool[0].obtainView();
	m.updateOpts({		
		x: data.x + offsetX,
		y: Global.fieldHeight - Global.floorY - data.y,
		HP: data.HP + (roundTrip * 10 * data.HP) / 100,
		visible: true,
	});
	m.reset();
	m.addBody();
};

var addCanon = function (data, offsetX, roundTrip) {
	var m = enemiesPool[1].obtainView();
	m.updateOpts({		
		x: data.x + offsetX,
		y: Global.fieldHeight - Global.floorY - data.y,
		HP: data.HP + (roundTrip * 10 * data.HP) / 100,
		visible: true,
	});
	m.reset();
	m.addBody();
};

var addTank = function (data, offsetX, roundTrip) {
	var m = enemiesPool[2].obtainView();
	m.updateOpts({		
		x: data.x + offsetX,
		y: Global.fieldHeight - Global.floorY - data.y,
		HP: data.HP + (roundTrip * 10 * data.HP) / 100,
		visible: true,
	});
	m.reset();
	m.addBody();
};

var addTower = function (data, styleIdx, offsetX, roundTrip) {	
	imageUrl = data.image.replace('1', styleIdx.toString());
	
	var t = towersPool.obtainView();
	t.updateOpts({
		image: imageUrl,
		x: data.x + offsetX,
		y: (Global.fieldHeight - Global.floorY - data.y - data.height / 2),
		width: data.width,
		height: data.height,
		offsetX: -(data.width / 2),
		offsetY: -(data.height / 2),
		anchorX: data.width / 2,
		anchorY: data.height / 2,
		startingHP: data.HP + (roundTrip * 10 * data.HP) / 100,
		topPadding: data.topPadding ? data.topPadding : 30,
		horizontalPadding: data.horizontalPadding ? data.horizontalPadding : 30
	});
	t.reset();
	t.addBody();
	
	if (data.block) {
		addBlock(data.block, styleIdx, offsetX, roundTrip, t);
	}
};

var addBlock = function (data, styleIdx, offsetX, roundTrip, tower) {
	imageUrl = data.image.replace('1', styleIdx.toString());
	
	var t = blocksPool.obtainView();
	t.updateOpts({
		image: imageUrl,
		x: data.x + offsetX,
		y: (Global.fieldHeight - Global.floorY - data.y - data.height / 2),
		width: data.width,
		height: data.height,
		offsetX: -(data.width / 2),
		offsetY: -(data.height / 2),
		anchorX: data.width / 2,
		anchorY: data.height / 2,
		startingHP: data.HP + (roundTrip * 10 * data.HP) / 100,
		tower: tower,
		bottomPadding: data.bottomPadding ? data.bottomPadding : 15,
		padding: data.padding ? data.padding : 15
	});
	t.reset();
	t.addBody();
};

var level1 = {
	tower_1: {
		image: 'resources/images/buildings/tower1a.png',
		x: 500,
		y: -100,
		width: 128,
		height: 256,
		HP: 5
	},	
	mortar_1: {
		x: 500,
		y: 256 - 100,
		HP: 1
	}
};

var level2 = {
	tower_1: {
		image: 'resources/images/buildings/tower1a.png',
		x: 500,
		y: 0,
		width: 128,
		height: 256,
		HP: 7,
		block: {
			image: 'resources/images/buildings/towerroof1a.png',
			x: 500,
			y: 256,
			width: 99,
			height: 149,
			HP: 1
		},
	},	
	canon_1: {
		x: 500,
		y: 256,
		HP: 2
	}
};

var level3 = {
	tower_2: {
		image: 'resources/images/buildings/gates1b.png',
		x: 608,
		y: 0,
		width: 192,
		height: 270,
		HP: 15,
		topPadding: 30,
		horizontalPadding: 40
	},
	mortar_1: {
		x: 500,
		y: 256 - 50,
		HP: 1
	},
	canon_2: {
		x: 722,
		y: 256 - 50,
		HP: 2
	},
	tower_1: {
		image: 'resources/images/buildings/tower1a.png',
		x: 500,
		y: -50,
		width: 128,
		height: 256,
		HP: 7,
		block: {
			image: 'resources/images/buildings/towerroof1a.png',
			x: 500,
			y: 256,
			width: 99,
			height: 149,
			HP: 1
		},
	},
	tower_3: {
		image: 'resources/images/buildings/tower1a.png',
		x: 722,
		y: -50,
		width: 128,
		height: 256,
		HP: 7,
		block: {
			image: 'resources/images/buildings/towerroof1a.png',
			x: 722,
			y: 256,
			width: 99,
			height: 149,
			HP: 1
		},
	}
};

var level4 = {
	tower_2: {
		image: 'resources/images/buildings/gates1b.png',
		x: 608,
		y: 0,
		width: 192,
		height: 270,
		HP: 15,
		topPadding: 30,
		horizontalPadding: 40,
	},
	mortar_1: {
		x: 500,
		y: 512 - 110,
		HP: 1
	},
	canon_2: {
		x: 722,
		y: 512 - 110,
		HP: 2
	},
	tank_3: {
		x: 608,
		y: 270,
		HP: 3
	},
	tower_1: {
		image: 'resources/images/buildings/tower1c.png',
		x: 500,
		y: -110,
		width: 128,
		height: 512,
		HP: 7,
		block: {
			image: 'resources/images/buildings/towerroof1a.png',
			x: 500,
			y: 512,
			width: 99,
			height: 149,
			HP: 1
		},
	},
	tower_3: {
		image: 'resources/images/buildings/tower1c.png',
		x: 722,
		y: -110,
		width: 128,
		height: 512,
		HP: 7,
		block: {
			image: 'resources/images/buildings/towerroof1a.png',
			x: 722,
			y: 512,
			width: 99,
			height: 149,
			HP: 1
		},
	}
};

var level5 = {
	tower_2: {
		image: 'resources/images/buildings/gates1c.png',
		x: 608,
		y: -10,
		width: 192,
		height: 510,
		HP: 15,
		topPadding: 30,
		horizontalPadding: 40
	},
	mortar_1: {
		x: 500,
		y: 512 - 110,
		HP: 1
	},
	mortar_2: {
		x: 722,
		y: 512 - 110,
		HP: 1
	},
	canon_3: {
		x: 608,
		y: 510,
		HP: 2
	},
	tower_1: {
		image: 'resources/images/buildings/tower1c.png',
		x: 500,
		y: -110,
		width: 128,
		height: 512,
		HP: 7,
		block: {
			image: 'resources/images/buildings/towerroof1a.png',
			x: 500,
			y: 512,
			width: 99,
			height: 149,
			HP: 1
		},
	},
	tower_3: {
		image: 'resources/images/buildings/tower1c.png',
		x: 722,
		y: -110,
		width: 128,
		height: 512,
		HP: 7,
		block: {
			image: 'resources/images/buildings/towerroof1a.png',
			x: 722,
			y: 512,
			width: 99,
			height: 149,
			HP: 1
		},
	},
	tower_4: {
		image: 'resources/images/buildings/tower1a.png',
		x: 416,
		y: 0,
		width: 128,
		height: 256,
		HP: 5
	},	
	mortar_4: {
		x: 416,
		y: 256,
		HP: 1
	},
	tower_5: {
		image: 'resources/images/buildings/tower1a.png',
		x: 810,
		y: 0,
		width: 128,
		height: 256,
		HP: 5
	},	
	tank_5: {
		x: 810,
		y: 256,
		HP: 3
	}
};

var levels = [level1, level2, level3, level4, level5];
