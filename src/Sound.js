import AudioManager;

var soundManager = null;

this.init = function() {
  if (soundManager != null) {
    return;
  }
  
  soundManager = new AudioManager({
    path: "resources/sounds/",
    files: {
      gun_fire: {
        volume: 1.0,
        loop: false,
        background: false
      },
      flaunch: {
        volume: 1.0,
        loop: false,
        background: false
      },     
      iceball: {
        volume: 1.0,
        loop: false,
        background: true
      }, 
      rlaunch: {
        volume: 1.0,
        loop: false,
        background: true
      },
      rumble: {
        volume: 1.0,
        loop: false,
        background: true
      },
      explodemini: {
        volume: 1.0,
        loop: false,
        background: true
      },
      explode: {
        volume: 1.0,
        loop: false,
        background: true
      },
      explosion_dull: {
        volume: 0.7,
        loop: false,
        background: true
      },
      fighter_sound: {
        volume: 1.0,
        loop: false,
        background: true
      },
    }
  });
};

this.play = function(name, isLoop) {
    soundManager.play(name, {loop: isLoop});  
};


