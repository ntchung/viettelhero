import ui.ImageView as ImageView;
import math.geom.Vec2D as Vec2D;

import src.cp as cp;
var v = cp.v;
		
import src.Global as Global;
import src.Effects as Effects;

exports = Class(ImageView, function (supr) {
	this.init = function (opts) {
		opts = merge(opts, {			
			width: 20,
			height: 20,
			offsetX: -10,
			offsetY: -10,
			anchorX: 10,
			anchorY: 10
		});
		
		supr(this, "init", [opts]);		
		
		this.collisionSize = 20;		
		this.reset();
	};	
	
	this.reset = function () {
		this.bounces = 0;		
	}
	
	this.destroy = function() {
		this.removeBody();
		bulletsPool.releaseView(this);
	}

	this.update = function (dt) {	
		Global.chipmunks2devkit(this, this.shape.body, 0, 0);
		
		var body = this.shape.body;
		this.style.r = Math.atan2(-body.vy, body.vx);
		
		if (!this.style.visible || this.style.y > Global.fieldHeight + this.style.height) {						
			this.destroy();
		}
	};
	
	this.scroll = function (dist) {
		this.style.x += dist;
		this.shape.body.p.x = this.style.x;
	};
	
	this.addBody = function(layer) {		
		var space = Global.space;

		var radius = this.collisionSize / 2;
		var mass = 3;
		var body = space.addBody(new cp.Body(mass, cp.momentForCircle(mass, 0, radius, v(0, 0))));
		body.setPos(Global.devkit2chipmunks(this, 0, 0));
		var circle = space.addShape(new cp.CircleShape(body, radius, v(0, 0)));
		circle.setElasticity(0.8);
		circle.setFriction(1);
		circle.setCollisionType(layer);
		circle.setLayers(layer);
		
		this.shape = circle;
		circle.actor = this;
	};
	
	this.removeBody = function () {
		var space = Global.space;	
			
		space.removeShape(this.shape);		
		this.shape = null;				
	};
	
	this.collideWithWall = function() {		
		++this.bounces;
		
		if (!Global.bulletsCanBounce || this.bounces > this._opts.maxBounces) {
			this.explode();
		}
	};
	
	this.explode = function () {
		this.style.visible = false;
		Effects.explode2(this, 0, 0, 0.5);
	}
});
