import animate;
import ui.SpriteView as SpriteView;
import math.geom.Vec2D as Vec2D;

import src.Sound as Sound;
import src.Global as Global;
import src.Bullet as Bullet;
import src.cp as cp;
var v = cp.v;

exports = Class(SpriteView, function (supr) {
	var State = {
		Idle: 0,
		Dying: 1,
		Reviving: 2,
		Attacking: 3
	};
	
	this.init = function (opts) {
		opts = merge(opts, {
			superview: rootView,
			url: 'resources/images/hero/hero',
			defaultAnimation: 'idle1',
			autoStart: true,
			loop: true,
			width: 170,
			height: 116,
			offsetX: -85,
			offsetY: -116
		});
		
		supr(this, "init", [opts]);
		
		this.bomber = new SpriteView({
			superview: rootView,
			url: 'resources/images/bomber/bomber',
			defaultAnimation: 'fly',
			autoStart: true,
			loop: true,
			width: 128,
			height: 63,
			offsetX: -64,
			offsetY: -58,
			x: 0,
			y: -500,
			visible: false,
		});

		this.touchTime = 0;
		this.coolDown = 0;
		this.collisionSize = 58;
		this.gunLength = 77;
		this.pivotX = 0;
		this.pivotY = 38;
		this.aimDir = new Vec2D({ x: 1.0, y: 0.0 });
		
		this.airStrikeTime = 0;
		this.airStrikeInterval = 0;
		
		this.state = State.Idle;		
		this.isDyingCompletely = false;
		this.dyingTimeOut = 3000;
	};

	this.lookAt = function (point) {
		if (this.state != State.Dying) {
			var dir = new Vec2D({
				x: point.x - (this.style.x + this.pivotX),
				y: point.y - (this.style.y - this.pivotY)
			});
			
			if (point.x < this.style.x + this.pivotX) {
				dir.x = 0.0;
				dir.y = -1.0;
			} else if (point.y > this.style.y - this.pivotY) {
				dir.x = 1.0;
				dir.y = 0.0;
			}
	
			var angle = Math.floor((-Math.atan2(dir.y, dir.x) * 2.0 / Math.PI) * 10);
			var mag = dir.getMagnitude();
			this.aimDir.x = dir.x / mag;
			this.aimDir.y = dir.y / mag;
	
			this.startAnimation('idle' + (angle + 1), { loop: true });
		}
	};

	this.launchRocket = function () {		
		if (this.state != State.Dying) {
			Sound.play('gun_fire');
			
			var force = Math.min(Math.max(this.touchTime, Global.minimumChargeToAttack) * 3, 3000);
			
			var b = bulletsPool.obtainView();
			b.updateOpts({	
				image: 'resources/images/bullets/bullet2-fly-0001.png',
				x: this.style.x + this.pivotX + this.aimDir.x * this.gunLength,
				y: this.style.y - this.pivotY + this.aimDir.y * this.gunLength,
				width: 24,
				height: 24,
				offsetX: -12,
				offsetY: -12,
				anchorX: 12,
				anchorY: 12,
				maxBounces: 2,
				visible: true,
			});		
			b.reset();
			b.addBody(Global.HERO_BULLET);
			
			var impulse = v(this.aimDir.x * force, -this.aimDir.y * force);		
			b.shape.body.applyImpulse(impulse, v(0, 0));
			b.style.r = Math.atan2(-impulse.vy, impulse.vx);
		}		
	};
	
	this.revive = function() {
		this.state = State.Reviving;	
	};
	
	this.attack = function() {
		if (this.state != State.Dying) {
			if (this.coolDown <= 0) {
				this.launchRocket();
				this.touchTime = 0;
				this.coolDown = Global.attackCoolDown;
			} else {
				this.state = State.Attacking;
			}
		}
	};
	
	this.startCharging = function() {
		this.touchTime = 0;
	};
	
	this.update = function(dt) {
		this.touchTime += dt;
		if (this.coolDown > 0) {
			this.coolDown -= dt;
		}
		
		if (this.state == State.Dying) {
			Global.chipmunks2devkit(this, this.shape.body, 0, -this.collisionSize / 2);
			
			if (!this.isDyingCompletely) {
				this.dyingTimeOut -= dt;
				this.isDyingCompletely = this.dyingTimeOut <= 0 || (Math.abs(this.shape.body.vx) < 0.001 && Math.abs(this.shape.body.vy) < 0.001);
			}
		} else if (this.state == State.Reviving) {			
			this._opts.defaultAnimation = 'idle1';		
			this.startAnimation('idle1');
			
			this.style.x = Global.heroPositionX;
			this.style.y = Global.fieldHeight - Global.floorY,
			
			this.removeBody();
			this.addBody();	
			
			this.state = State.Idle;
			this.dyingTimeOut = 3000;
		} else if (this.state == State.Attacking) {
			if (this.coolDown <= 0) {
				this.launchRocket();
				this.touchTime = 0;
				this.state = State.Idle;
				this.coolDown = Global.attackCoolDown;
			}
		}
		
		// Update air strike
		if (this.airStrikeTime > 0) {
			this.airStrikeTime -= dt;
			
			this.airStrikeInterval -= dt;
			if (this.airStrikeInterval  < 0) {
				this.airStrikeInterval = 200;
				
				var force = Math.min(this.touchTime * 3, 3000);
			
				var b = bulletsPool.obtainView();
				b.updateOpts({	
					image: 'resources/images/bullets/bullet1-fly-0001.png',
					x: this.bomber.style.x,
					y: this.bomber.style.y,
					width: 32,
					height: 32,
					offsetX: -16,
					offsetY: -16,
					anchorX: 16,
					anchorY: 16,
					maxBounces: 0,
					visible: true,
				});		
				b.reset();
				b.addBody(Global.HERO_BULLET);
				
				var impulse = v(Math.random() * 500 + 500, 0);		
				b.shape.body.applyImpulse(impulse, v(0, 0));
				b.style.r = Math.atan2(-impulse.vy, impulse.vx);
			}
		}
	};
	
	this.addBody = function() {		
		var space = Global.space;

		var radius = this.collisionSize / 2;
		var mass = 80;
		var body = space.addBody(new cp.Body(mass, Infinity));
		body.setPos(Global.devkit2chipmunks(this, 0, -radius));
		var circle = space.addShape(new cp.CircleShape(body, radius, v(0, 0)));
		circle.setElasticity(0.5);
		circle.setFriction(1);
		circle.setCollisionType(Global.HERO);
		circle.setLayers(Global.HERO);
		
		this.shape = circle;
		circle.actor = this;
	};
	
	this.removeBody = function () {
		var space = Global.space;	
			
		space.removeShape(this.shape);		
		this.shape = null;				
	};
	
	this.applyImpulse = function(impulse) {				
		this.shape.body.applyImpulse(impulse, v(0, 0));
		
		if (this.state != State.Dying) {
			this.state = State.Dying;
			this.airStrikeTime = 0;
			
			this._opts.defaultAnimation = 'liedown';		
			this.startAnimation('dying');
			
			this.isDyingCompletely  = false;
		}
	};
	
	this.isDying = function () {
		return this.state == State.Dying;
	};
	
	this.isVanquished = function() {
		return this.state == State.Dying && this.isDyingCompletely;
	};
	
    this.launchAirStrike = function () {
		
		if (this.state != State.Dying) {
			Sound.play('fighter_sound');
			
			this.airStrikeTime = 2000;
			this.airStrikeInterval = 200;
			
			animate(this.bomber).clear()
				.now({visible: true, x: -100, y: Global.fieldHeight / 3}, 0, animate.linear)
				.then({x: 800, y: -50}, this.airStrikeTime, animate.linear);
		}  	    
    };
});
