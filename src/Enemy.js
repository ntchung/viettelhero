import ui.SpriteView as SpriteView;
import math.geom.Vec2D as Vec2D;

import src.Sound as Sound;
import src.Effects as Effects;
import src.Global as Global;
import src.Bullet as Bullet;
import src.cp as cp;

var v = cp.v;

exports = Class(SpriteView, function (supr) {
	var State = {
		Idle: 0,
		Attack: 1,
		Dying: 2
	};

	this.init = function (opts) {
		opts = merge(opts, {						
			defaultAnimation: 'idle',
			autoStart: true,
			loop: true			
		});

		supr(this, "init", [opts]);

		this.collisionSize = opts.collisionSize;
		this.barrelX = opts.barrelX;
		this.barrelY = opts.barrelY;
		this.dyingTimeOut = 3000;

		this.reset();
	};

	this.reset = function () {
		this.style.zIndex = 50;
		this.dyingTimeOut = 3000;
		
		this.HP = this._opts.HP;
		this.scoreValue = this._opts.scoreValue;

		this.waitBeforeAction = Math.random() * this._opts.waitBeforeAction + this._opts.firstWaitBeforeAction;
		this.bounces = 0;

		this.state = State.Idle;
		this._opts.defaultAnimation = 'idle';
	};

	this.addBody = function () {
		var space = Global.space;

		var radius = this.collisionSize / 2;
		var mass = this._opts.mass;
		var body = space.addBody(new cp.Body(mass, Infinity));
		body.setPos(Global.devkit2chipmunks(this, 0, -radius));
		var circle = space.addShape(new cp.CircleShape(body, radius, v(0, 0)));
		circle.setElasticity(0.5);
		circle.setFriction(1);
		circle.setCollisionType(Global.ENEMY);
		circle.setLayers(Global.ENEMY);

		this.shape = circle;
		circle.actor = this;
	};

	this.removeBody = function () {
		var space = Global.space;

		space.removeShape(this.shape);
		this.shape = null;
	};
	
	this.destroy = function() {
		this.removeBody();
		enemiesPool[this._opts.poolIdx].releaseView(this);
	}

	this.update = function (dt) {
		Global.chipmunks2devkit(this, this.shape.body, 0, -this.collisionSize / 2);

		if (!this.style.visible) {
			this.destroy();
		} else {
			var self = this;

			if (this.state == State.Idle) {
				if (GC.app.isFighting()) {
					this.waitBeforeAction -= dt;
					if (this.waitBeforeAction <= 0) {
						this.state = State.Attack;

						this.startAnimation('attack', {
							callback: function () {
								Sound.play(self._opts.attackSound);
								
								self.launchRocket(self.getComputerLaunchVelocity(3, self._opts.launchRocketVerticalForce));

								self.startAnimation('postattack', {
									callback: function () {
										self.waitBeforeAction = Math.random() * self._opts.waitBeforeAction + self._opts.firstWaitBeforeAction;
										self.state = State.Idle;
									}
								});
							}
						});
					}
				}
			} else if (this.state == State.Dying) {
				this.dyingTimeOut -= dt;
				
				if (this.bounces > 3
					|| Global.isBodyStopped(this.shape.body)
					|| this.dyingTimeOut <= 0) {
					Effects.explode1(this, 0, -this.collisionSize / 2, 0.75 * (this.style.width / 90));
					
					Sound.play('explode');

					this.destroy();
				}
			}
		}
	};
	
	this.scroll = function (dist) {
		this.style.x += dist;
		this.shape.body.p.x = this.style.x;
	};

	this.launchRocket = function (impulse) {
		var bsize = this._opts.bulletSize;

		var b = bulletsPool.obtainView();
		b.updateOpts({
			image: this._opts.bulletImage,
			x: this.style.x + this.barrelX,
			y: this.style.y - this.barrelY,
			width: bsize,
			height: bsize,
			anchorX: bsize / 2,
			anchorY: bsize / 2,
			offsetX: -bsize / 2,
			offsetY: -bsize / 2,
			maxBounces: this._opts.bulletMaxBounces,
			visible: true,
		});
		b.reset();
		b.addBody(Global.ENEMY_BULLET);

		b.shape.body.applyImpulse(impulse, v(0, 0));
		b.style.r = Math.atan2(-impulse.vy, impulse.vx);
	};

	this.applyImpulse = function (impulse) {
		if (this.state != State.Dying) {
			--this.HP;
			if (this.HP <= 0) {
				GC.app.addScore(this.scoreValue);
				this.state = State.Dying;
				
				this._opts.defaultAnimation = 'liedown';
				this.startAnimation('dying');
				
				if (this._opts.instantDeath) {
					this.bounces = 100;
				} else {
					this.shape.body.applyImpulse(impulse, v(0, 0));
					this.style.zIndex = 1000;
				}
			}
		}
	};

	this.collideWithWall = function () {
		if (this.state == State.Dying) {
			++this.bounces;
		}
	}

    this.getComputerLaunchVelocity = function (bulletMass, verticalForce) {
		var startingHeight = Global.fieldHeight - Global.floorY - this.style.y;
        var targetLocation = v(Global.heroPositionX, Global.floorY);
        var verticalVelocity = verticalForce;
        var startingVelocity = v(0, verticalVelocity);
        var timestepsToTop = this.getTimestepsToTop(startingVelocity);
		var timestepsToBottom = Math.abs(startingHeight / (Global.space.gravity.y * 60)); 
        var targetEdgePos = targetLocation.x;

		var width = this._opts.inaccuracy;
        var distanceToTargetEdge = targetEdgePos - this.shape.body.p.x + (Math.random() * width * 2) - width;
        var horizontalVelocity = distanceToTargetEdge * bulletMass * (verticalForce * 0.0015) / (timestepsToTop + timestepsToBottom) * 60;
        return v(horizontalVelocity, verticalVelocity);
    };

	this.getTimestepsToTop = function (startingVelocity) {
        var t = 1 / 60.0;
        var stepVelocityY = t * startingVelocity.y; // m/s
        var stepGravityY = t * t * Global.space.gravity.y; // m/s/s

        var n = -stepVelocityY / stepGravityY - 1;
        return n;
    };
});
